.. MyProject documentation master file, created by
   sphinx-quickstart on Mon Sep  6 15:40:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MyProject's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   example

   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
  
Modules
=======

.. automodule:: mymodule.default
   :members: